#!/bin/bash

usage() { echo "Usage: $0 [-p <string> (project name)] [-r <string> (folder name)] [-u <string> (git url)] [-t <string> (git template folder name)] [-l <string> (git dotfiles folder name)] [-g <string> (git group name)]" 1>&2; exit 1; }

while getopts s:p:r:u:t:l:g: option
do
    case "${option}"
    in
        p) FILE_NAME=${OPTARG};;
        r) GIT_ARTIFACT_FOLDER_NAME=${OPTARG};;
        u) GIT_URL_PREFIX=${OPTARG};;
        t) GIT_TEMPLATE_PREFIX=${OPTARG};;
        l) GIT_DOTFILES_LOCATION=${OPTARG};;
        g) GIT_GROUP_PREFIX=${OPTARG};;
        \?) usage;;
    esac
done

if [[ -z "$CI_PROJECT_DIR" ]]; then
    CI_PROJECT_DIR=$PWD
fi

if [ ! -d "$GIT_ARTIFACT_FOLDER_NAME" ]; then
  mkdir $GIT_ARTIFACT_FOLDER_NAME
fi

cd $GIT_ARTIFACT_FOLDER_NAME

curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/$GIT_DOTFILES_LOCATION/raw/main/.gitattributes
curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/$GIT_DOTFILES_LOCATION/raw/main/.gitignore
curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/$GIT_DOTFILES_LOCATION/raw/main/.dockerignore
curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/$GIT_DOTFILES_LOCATION/raw/main/gitlab-ci/docker/.default-ci.yml
curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/docker/raw/main/Dockerfile
sed -i -e 's#REPLACE_EXAMPLE_PROJECTNAME#'"$FILE_NAME"'#g' .default-ci.yml
sed -i -e 's#REPLACE_EXAMPLE_PROJECTNAME#'"$FILE_NAME"'#g' Dockerfile
sed -i -e 's#REPLACE_EXAMPLE_SPECIFIC_GITLAB_PROJECTNAME#'"$GIT_GROUP_PREFIX/$GIT_TEMPLATE_PREFIX/$GIT_DOTFILES_LOCATION"'#g' .default-ci.yml

mv .default-ci.yml .gitlab-ci.yml

echo $CI_PROJECT_DIR/$GIT_ARTIFACT_FOLDER_NAME
